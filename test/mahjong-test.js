var assert = require("assert");

describe('Mahjong', function(){
    var mahjong = require("./../mahjong"),
        tile = mahjong.tile;
    describe('#win()', function(){

        describe('for special types', function(){
            it('should win with 国士无双', function() {
                var tiles = [
                    tile("1 wan 1"),
                    tile("9 wan 1"),
                    tile("1 tiao 1"),
                    tile("9 tiao 1"),
                    tile("1 tong 1"),
                    tile("9 tong 1"),
                    tile("dong 1"),
                    tile("nan 1"),
                    tile("nan 2"),
                    tile("xi 1"),
                    tile("bei 1"),
                    tile("zhong 1"),
                    tile("fa 1"),
                    tile("bai 1")
                ];
                var result = mahjong.win(tiles);
                assert.equal(1, result.length);
                assert.equal("a state scholar of no equal", result[0].type);
            });

            it('should win with 七对', function() {
                var tiles = [
                    tile("1 wan 1"),
                    tile("1 wan 2"),
                    tile("3 wan 1"),
                    tile("3 wan 4"),
                    tile("dong 1"),
                    tile("dong 2"),
                    tile("bai 1"),
                    tile("bai 2"),
                    tile("2 tong 1"),
                    tile("2 tong 2"),
                    tile("3 tiao 1"),
                    tile("3 tiao 2"),
                    tile("9 tong 1"),
                    tile("9 tong 2")
                ];
                var result = mahjong.win(tiles);
                assert.equal(1, result.length);
                assert.equal("seven pairs", result[0].type);
            });
        });

        describe('for normal types', function(){
            it('should win in 4 ways', function() {
                var tiles = [
                    tile("1 wan 1"),
                    tile("1 wan 2"),
                    tile("1 wan 3"),
                    tile("2 wan 1"),
                    tile("2 wan 2"),
                    tile("2 wan 3"),
                    tile("3 wan 1"),
                    tile("3 wan 2"),
                    tile("3 wan 3"),
                    tile("4 wan 1"),
                    tile("4 wan 2"),
                    tile("4 wan 3"),
                    tile("5 wan 1"),
                    tile("5 wan 2")
                ];
                var result = mahjong.win(tiles);
                assert.equal(4, result.length);
            });

            it('should not win', function() {
                var tiles = [
                    tile("8 wan 1"),
                    tile("9 wan 1"),
                    tile("1 tiao 1"),
                    tile("2 tiao 1"),
                    tile("3 tiao 1"),
                    tile("4 tiao 1"),
                    tile("5 tiao 1"),
                    tile("5 tiao 2"),
                    tile("5 tiao 3"),
                    tile("6 tiao 1"),
                    tile("6 tiao 2"),
                    tile("6 tiao 3"),
                    tile("7 tiao 1"),
                    tile("7 tiao 2"),
                ];
                var result = mahjong.win(tiles);
                assert.equal(0, result.length);
            });
        });
    })
})