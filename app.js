var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var http = require('http').Server(app);
var io = require('socket.io')(http);

var mahjong = require('./mahjong');

io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('shuffle', function(){
    socket['deck'] = mahjong.shuffle();
    var base = socket['deck'].draw(13), newTile = socket['deck'].draw();
    socket['tiles'] = base.concat(newTile);
    var win = mahjong.win(socket['tiles']).length != 0;
    socket.emit('shuffle', {
      base: base,
      newTile: newTile,
      remaining: socket['deck'].length,
      win: win
    });
  });

  socket.on('new tile', function(){
    var newTile = socket['deck'].draw();
    socket['tiles'].push(newTile);
    var win = mahjong.win(socket['tiles']).length != 0;
    socket.emit('new tile', {
      newTile: newTile,
      remaining: socket['deck'].length,
      win: win
    });
  });

  socket.on('kick tile', function(seq){
    socket['tiles'] = socket['tiles'].filter(function(tile){
      return tile.seq != seq;
    });
    socket.emit('kick tile');
  })

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(3469, function(){
  console.log('listening on *:3469');
});
