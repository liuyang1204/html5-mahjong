var allTiles = [];
var tilesMap = {};

var baseTileSufix = "\udc00";
function tileAt(offset) {
  return "\ud83c" + String.fromCharCode(baseTileSufix.charCodeAt() + offset);
}

//Wan, Tiao, Tong, Zi
for(var i = 7; i <= 40; i++) {
  var normalCount = 4, doraCount = 0;
  if(i == 11 || i == 20) {
    normalCount = 3;
    doraCount = 1;
  }
  if(i == 29) {
    normalCount = 2;
    doraCount = 2;
  }
  for(var j=0; j < normalCount; j++) {
    var tile = {
      charNum: i - 6,
      charactor: tileAt(i % 34),
      dora: false
    };
    allTiles.push(tile);
    tile.seq = allTiles.length;
  }
  for(var j=0; j < doraCount; j++) {
    var tile = {
      charNum: i - 6,
      charactor: tileAt(i % 34),
      dora: true
    };
    allTiles.push(tile);
    tile.seq = allTiles.length;
  }
}

var shuffle = require('shuffle');

var TYPE = {
  JIANG : 1,
  KE: 2,
  SHUN: 3
}

function trasformMatch(consumed, types, remaining, countKE, countJIANG, countSHUN) {
  var consumedN = consumed.slice(),
      typesN = types.slice(),
      remainingN = remaining.slice();

  //KE at most 1, are will surely match
  for(var i=0; i<countKE; i++) {
    consumedN = consumedN.concat(remainingN[0], remainingN[1], remainingN[2]);
    typesN = typesN.concat(TYPE.KE);
    remainingN = remainingN.slice(3);
  }

  //JIANG at most 1, are will surely match
  for(var i=0; i<countJIANG; i++) {
    consumedN = consumedN.concat(remainingN[0], remainingN[1]);
    typesN = typesN.concat(TYPE.JIANG);
    remainingN = remainingN.slice(2);
  }

  //However, SHUN has possibility of "not match"
  for(var k=0; k<countSHUN; k++) {
    //try match SHUN
    var shun1 = remainingN[0], shun2 = undefined, shun3 = undefined;
    for(var i=1; i<remainingN.length; i++) {
      var tile = remainingN[i];
      if(shun2 == undefined && tile.charNum == shun1.charNum + 1) {
        shun2 = tile;
      }
      if(shun3 == undefined && tile.charNum == shun1.charNum + 2) {
        shun3 = tile;
        break;
      }
    }
    if(shun2 && shun3) {
      //match SHUN
      consumedN = consumedN.concat(shun1, shun2, shun3);
      typesN = typesN.concat(TYPE.SHUN);
      remainingN = remainingN.filter(function(tile){
        return shun1 != tile && shun2 != tile && shun3 != tile;
      });
    } else { //not match
      return [];
    }
  }
  return winTypes(consumedN, typesN, remainingN);
}
//remaining should be sorted
function winTypes(consumed, types, remaining) {
  var result = [];
  if (remaining.length == 0) {
    //success
    return [{sequence: consumed, units: types, type: "normal"}];
  }
  var first = remaining[0];

  //count tiles of the same charactor of first tile
  var count = 1;
  for(var i=1; i<remaining.length; i++) {
    if(remaining[i].charNum == first.charNum) {
      count ++;
    } else {
      break;
    }
  }

  //WAN 1 - 9
  //TIAO 10 - 18
  //TONG 19 - 27
  //ZI 28 - 34

  //possibility
  var pJIANG = types.indexOf(TYPE.JIANG) == -1,
      pSHUN = (first.charNum) < 28 && (((first.charNum - 1) % 9) < 7);
  switch(count) {
    case 1:
      if(pSHUN && remaining.length >= 3) {
        //SHUN
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 0, 1));
      }
      break;
    case 2:
      if(pJIANG) {
        //JIANG
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 1, 0));
      }
      if(pSHUN && remaining.length >= 6) {
        //SHUN * 2
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 0, 2));
      }
      break;
    case 3:
      //KE
      [].push.apply(result, trasformMatch(consumed, types, remaining, 1, 0, 0));
      if(pSHUN && pJIANG && remaining.length >= 5){
        //JIANG + SHUN
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 1, 1));
      }
      if(pSHUN && remaining.length >= 9) {
        //SHUN * 3
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 0, 3));
      }
      break;
    case 4:
      if(pSHUN && remaining.length >= 6) {
        //KE + SHUN
        [].push.apply(result, trasformMatch(consumed, types, remaining, 1, 0, 1));
      }
      if(pSHUN && pJIANG && remaining.length >= 8) {
        //JIANG + SHUN * 2
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 1, 2));
      }
      if(pSHUN && remaining.length >= 12) {
        //SHUN * 4
        [].push.apply(result, trasformMatch(consumed, types, remaining, 0, 0, 4));
      }
      break;
  }
  return result;
}

var specialTypes = [
  {
    name: "seven pairs", //七对
    match: function(fourteenTiles) {
      for(var i=0; i<7; i++) {
        if(fourteenTiles[2 * i].charNum != fourteenTiles[2 * i + 1].charNum) {
          return false;
        }
        if(i < 6 && fourteenTiles[2 * i + 1].charNum == fourteenTiles[2 * i + 2]) {
          return false; //seven pairs can not contain 4 same charactors
        }
      }
      return true;
    }
  },
  {
    name: "a state scholar of no equal", //国士无双 TAT
    match: function(fourteenTiles) {
      var tileCharNums = [1, 9, 10, 18, 19, 27, 28, 29, 30, 31, 32, 33, 34];
      var extra = undefined;
      for (var i = 0; i < fourteenTiles.length; i++) {
        var idx = i, tile = fourteenTiles[i];
        if(extra) {
          idx = i - 1;
        }
        if(tile.charNum != tileCharNums[idx]) {
          if(extra != undefined || i == 0) {
            return false;
          }
          if(tile.charNum != fourteenTiles[i-1].charNum) {
            return false;
          }
          extra = fourteenTiles[i];
        }
      }
      return true;
    }
  }
];

module.exports = {
  allTiles: function() {
    return allTiles;
  },
  tile: function(exp){
    var words = exp.trim().split(/[\s]+/), seq = Number(words[words.length-1]), charNum;
    switch(words[0]) {
      case "dong":
        charNum = 28;
        break;
      case "nan":
        charNum = 29;
        break;
      case "xi":
        charNum = 30;
        break;
      case "bei":
        charNum = 31;
        break;
      case "zhong":
        charNum = 32;
        break;
      case "fa":
        charNum = 33;
        break;
      case "bai":
        charNum = 34;
        break;
      default:
        var num = Number(words[0]);
        var type = words[1];
        switch(type) {
          case "wan":
            charNum = num;
            break;
          case "tiao":
            charNum = 9 + num;
            break;
          case "tong":
            charNum = 18 + num;
            break;
        }
        break;
    }
    return allTiles[(charNum-1) * 4 + seq -1];
  },
  shuffle: function() {
    return shuffle.shuffle({deck: allTiles});
  },

  win: function(fourteenTiles) {
    if(fourteenTiles.length != 14) {
      return [];
    }

    fourteenTiles.sort(function(a, b){
      return a.seq - b.seq;
    });

    for (var i = 0; i < specialTypes.length; i++) {
      if(specialTypes[i].match(fourteenTiles)){
        return [{sequence: fourteenTiles, type:specialTypes[i].name}];
      }
    }
    return winTypes([], [], fourteenTiles);
  }
};